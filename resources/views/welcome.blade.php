<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            a {
                color:black;
                text-decoration: none;
                font-size: 30px;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
                <a href="/deficiency">Deficiência</a> ||||
                <a href="/education">Educação</a> ||||
                <a href="/profession">Profissão</a> ||||
                <a href="/user">Usuários</a> ||||
                <a href="/departament">Departamento</a> ||||
                <a href="/employee">Funcionário</a> ||||
                <a href="/therapy">Terapias</a> ||||
                <a href="/pacient">Pacientes</a> ||||
                <a href="/reports">Relatórios</a> ||||
            </div>
        </div>
    </body>
</html>
